import { createRouter, createWebHistory } from 'vue-router'
import ListSurvey from '@/views/ListSurvey.vue'

const routes = [
  {
    path: '/',
    name: 'ListSurvey',
    component: ListSurvey
  },
  {
    path: '/survey/:code',
    name: 'SurveyDetail',
    component: () => import('@/views/SurveyDetail.vue')
  },
  {
    path: '/404',
    name: 'NotFound',
    component: () => import('@/views/NotFound.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
